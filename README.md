<div align="center">
<h1> DEVELOPER MENU </h1>
</div>

This is simple menu resource that allows you to easy enable some functions like god mode, give ammo, spawn vehicle and many more by clicking option in this menu.

## Installing :rocket:
:arrow_down: or clone this repository from [gitlab](gitlab.com/Czesiek2000/developer_menu)

After downloading this repository, unpack it
Add following line to main `index.js` file

```js
require('developer_menu');
```

To clone this repository:
```bash
git clone gitlab.com/Czesiek2000/developer_menu
```

## Preview :camera:
![preview](images/dev_menu.png)

![preview](images/dev_menu_player.png)
![preview](images/dev_menu_vehicle.png)

## Usage
To open this menu press `F6` and then you will see some options available. 

### Features
* Add god mod, armor, hp to player
* Add super jump
* Termal vision
* Spawn car
* Spawn custom car,
* Repair, remove, lock, explode, flip, freeze, remove vehicle
* Get weapon with explosive ammo
* Explosive mele
* Change time to specific,
* Add subtract hour
* Change weather


If you :heart: or :thumsup: this resource, don't forget to leave :star: and leave :speech_balloon: on RAGEMP [forum](https://rage.mp/forums/)