exports = [
    {
        description: 'Metallic Black',
        
        rgbColor: [13, 17, 22]
    },

    {   
        description: 'Metallic Graphite Black',
        
        rgbColor: [28, 29, 33]
    },

    {   
        description: 'Metallic Black Steal',
        
        rgbColor: [50, 56, 61]
    },

    {   
        description: 'Metallic Dark Silver',
        
        rgbColor: [69, 75, 79]
    },

    {   
        description: 'Metallic Silver',
        
        rgbColor: [153, 157, 160]
    },

    {   
        description: 'Metallic Blue Silver',
        
        rgbColor: [194, 196, 198]
    },

    {   
        description: 'Metallic Steel Gray',
        
        rgbColor: [151, 154, 151]
    },

    {   
        description: 'Metallic Shadow Silver',
        
        rgbColor: [99, 115, 128]
    },

    {
       description: 'Metallic Stone Silver',
        
        rgbColor: [99, 98, 92]
    },

    {    
        description: 'Metallic Midnight Silver',
        
        rgbColor: [60, 63, 71]
    },

    {
        
        description: 'Metallic Gun Metal',
        
        rgbColor: [68, 78, 84]
    },

    {
        
        description: 'Metallic Anthracite Grey',
        
        rgbColor: [29, 33, 41]
    },

    {
        
        description: 'Matte Black',
        
        rgbColor: [19, 24, 31]
    },

    {
        
        description: 'Matte Gray',
        
        rgbColor: [38, 40, 42]
    },

    {
        
        description: 'Matte Light Grey',
        
        rgbColor: [81, 85, 84]
    },

    {
        
        description: 'Util Black',
        
        rgbColor: [21, 25, 33]
    },

    {
        
        description: 'Util Black Poly',
        
        rgbColor: [30, 36, 41]
    },

    {
        
        description: 'Util Dark silver',
        
        rgbColor: [51, 58, 60]
    },

    {
        
        description: 'Util Silver',
        
        rgbColor: [140, 144, 149]
    },
    
    {
        
        description: 'Util Gun Metal',
        
        rgbColor: [57, 67, 77]
    },

    {
        
        description: 'Util Shadow Silver',
        
        rgbColor: [80, 98, 114]
    },

    {
        
        description: 'Worn Black',
        
        rgbColor: [30, 35, 47]
    },

    {
        
        description: 'Worn Graphite',
        
        rgbColor: [54, 58, 63]
    },

    {
        
        description: 'Worn Silver Grey',
        
        rgbColor: [160, 161, 153]
    },

    {
        
        description: 'Worn Silver',
        
        rgbColor: [211, 211, 211]
    },

    {
        
        description: 'Worn Blue Silver',
        
        rgbColor: [183, 191, 202]
    },

    {
        
        description: 'Worn Shadow Silver',
        
        rgbColor: [119, 135, 148]
    },

    {
        
        description: 'Metallic Red',
        
        rgbColor: [192, 14, 26]
    },

    {
        
        description: 'Metallic Torino Red',
        
        rgbColor: [218, 25, 24]
    },

    {
        
        description: 'Metallic Formula Red',
        
        rgbColor: [182, 17, 27]
    },

    {
        
        description: 'Metallic Blaze Red',
        
        rgbColor: [165, 30, 35]
    },

    {
        
        description: 'Metallic Graceful Red',
        
        rgbColor: [123, 26, 34]
    },

    {
        
        description: 'Metallic Garnet Red',
        
        rgbColor: [142, 27, 31]
    },

    {
        
        description: 'Metallic Desert Red',
        
        rgbColor: [111, 24, 24]
    },

    {
        
        description: 'Metallic Cabernet Red',
        
        rgbColor: [73, 17, 29]
    },

    {
        
        description: 'Metallic Candy Red',
        
        rgbColor: [182, 15, 37]
    },

    {
        
        description: 'Metallic Sunrise Orange',
        
        rgbColor: [212, 74, 23]
    },

    {
        
        description: 'Metallic Classic Gold',
        
        rgbColor: [194, 148, 79]
    },

    {
        
        description: 'Metallic Orange',
        
        rgbColor: [247, 134, 22]
    },

    {
        
        description: 'Matte Red',
        
        rgbColor: [207, 31, 33]
    },

    {
        
        description: 'Matte Dark Red',
        
        rgbColor: [115, 32, 33]
    },

    {
        
        description: 'Matte Orange',
        
        rgbColor: [242, 125, 32]
    },

    {
        
        description: 'Matte Yellow',
        
        rgbColor: [255, 201, 31]
    },

    {
        
        description: 'Util Red',
        
        rgbColor: [156, 16, 22]
    },

    {
        
        description: 'Util Bright Red',
        
        rgbColor: [222, 15, 24]
    },

    {
        
        description: 'Util Garnet Red',
        
        rgbColor: [143, 30, 23]
    },

    {
        
        description: 'Worn Red',
        
        rgbColor: [169, 71, 68]
    },

    {
        
        description: 'Worn Golden Red',
        
        rgbColor: [177, 108, 81]
    },
    
    {
        
        description: 'Worn Dark Red',
        
        rgbColor: [55, 28, 37]
    },

    {
        
        description: 'Metallic Dark Green',
        
        rgbColor: [19, 36, 40]
    },
    
    {
        
        description: 'Metallic Racing Green',
        
        rgbColor: [18, 46, 43]
    },

    {
        
        description: 'Metallic Sea Green',
        
        rgbColor: [18, 56, 60]
    },

    {
        
        description: 'Metallic Olive Green',
        
        rgbColor: [49, 66, 63]
    },

    {
        
        description: 'Metallic Green',
        
        rgbColor: [21, 92, 45]
    },

    {
        
        description: 'Metallic Gasoline Blue Green',
        
        rgbColor: [27, 103, 112]
    },

    {
        
        description: 'Matte Lime Green',
        
        rgbColor: [102, 184, 31]
    },

    {
        
        description: 'Util Dark Green',
        
        rgbColor: [34, 56, 62]
    },

    {
        
        description: 'Util Green',
        
        rgbColor: [29, 90, 63]
    },

    {
        
        description: 'Worn Dark Green',
        
        rgbColor: [45, 66, 63]
    },

    {
        
        description: 'Worn Green',
        
        rgbColor: [69, 89, 75]
    },

    {
        
        description: 'Worn Sea Wash',
        
        rgbColor: [101, 134, 127]
    },

    {
        
        description: 'Metallic Midnight Blue',
        
        rgbColor: [34, 46, 70]
    },

    {
        
        description: 'Metallic Dark Blue',
        
        rgbColor: [35, 49, 85]
    },

    {
        
        description: 'Metallic Saxony Blue',
        
        rgbColor: [48, 76, 126]
    },

    {
        
        description: 'Metallic Blue',
        
        rgbColor: [71, 87, 143]
    },

    {
        
        description: 'Metallic Mariner Blue',
        
        rgbColor: [99, 123, 167]
    },

    {
        
        description: 'Metallic Harbor Blue',
        
        rgbColor: [57, 71, 98]
    },

    {
        
        description: 'Metallic Diamond Blue',
        
        rgbColor: [214, 231, 241]
    },
    
    {
        
        description: 'Metallic Surf Blue',
        
        rgbColor: [118, 175, 190]
    },

    {
        
        description: 'Metallic Nautical Blue',
        
        rgbColor: [52, 94, 114]
    },
    
    {
        
        description: 'Metallic Bright Blue',
        
        rgbColor: [11, 156, 241]
    },

    {
        
        description: 'Metallic Purple Blue',
        
        rgbColor: [47, 45, 82]
    },

    {
        
        description: 'Metallic Spinnaker Blue',
        
        rgbColor: [40, 44, 77]
    },

    {
        
        description: 'Metallic Ultra Blue',
        
        rgbColor: [35, 84, 161]
    },

    {
        
        description: 'Metallic Bright Blue',
        
        rgbColor: [110, 163, 198]
    },

    {
        
        description: 'Util Dark Blue',
        
        rgbColor: [17, 37, 82]
    },

    {
        
        description: 'Util Midnight Blue',
        
        rgbColor: [27, 32, 62]
    },

    {
        
        description: 'Util Blue',
        
        rgbColor: [39, 81, 144]
    },

    {
        
        description: 'Util Sea Foam Blue',
        
        rgbColor: [96, 133, 146]
    },

    {
        
        description: 'Util Lightning blue',
        
        rgbColor: [36, 70, 168]
    },

    {
        
        description: 'Util Maui Blue Poly',
        
        rgbColor: [66, 113, 225]
    },

    {
        
        description: 'Util Bright Blue',
        
        rgbColor: [59, 57, 224]
    },

    {
        
        description: 'Matte Dark Blue',
        
        rgbColor: [31, 40, 82]
    },

    {
        
        description: 'Matte Blue',
        
        rgbColor: [37, 58, 167]
    },

    {
        
        description: 'Matte Midnight Blue',
        
        rgbColor: [28, 53, 81]
    },

    {
        
        description: 'Worn Dark blue',
        
        rgbColor: [76, 95, 129]
    },

    {
        
        description: 'Worn Blue',
        
        rgbColor: [88, 104, 142]
    },

    {
        
        description: 'Worn Light blue',
        
        rgbColor: [116, 181, 216]
    },

    {
        
        description: 'Metallic Taxi Yellow',
        
        rgbColor: [255, 207, 32]
    },
    
    {
        
        description: 'Metallic Race Yellow',
        
        rgbColor: [251, 226, 18]
    },

    {
        
        description: 'Metallic Bronze',
        
        rgbColor: [145, 101, 50]
    },

    {
        
        description: 'Metallic Yellow Bird',
        
        rgbColor: [224, 225, 61]
    },

    {
        
        description: 'Metallic Lime',
        
        rgbColor: [152, 210, 35]
    },

    {
        
        description: 'Metallic Champagne',
        
        rgbColor: [155, 140, 120]
    },

    {
        
        description: 'Metallic Pueblo Beige',
        
        rgbColor: [80, 50, 24]
    },

    {
        
        description: 'Metallic Dark Ivory',
        
        rgbColor: [71, 63, 43]
    },

    {
        
        description: 'Metallic Choco Brown',
        
        rgbColor: [34, 27, 25]
    },

    {
        
        description: 'Metallic Golden Brown',
        
        rgbColor: [101, 63, 35]
    },

    {
        
        description: 'Metallic Light Brown',
        
        rgbColor: [119, 92, 62]
    },

    {
        
        description: 'Metallic Straw Beige',
        
        rgbColor: [172, 153, 117]
    },

    {
        
        description: 'Metallic Moss Brown',
        
        rgbColor: [108, 107, 75]
    },

    {
        
        description: 'Metallic Biston Brown',
        
        rgbColor: [64, 46, 43]
    },

    {
        
        description: 'Metallic Beechwood',
        
        rgbColor: [164, 150, 95]
    },

    {
        
        description: 'Metallic Dark Beechwood',
        
        rgbColor: [70, 35, 26]
    },

    {
        
        description: 'Metallic Choco Orange',
        
        rgbColor: [117, 43, 25]
    },

    {
        
        description: 'Metallic Beach Sand',
        
        rgbColor: [191, 174, 123]
    },

    {
        
        description: 'Metallic Sun Bleeched Sand',
        
        rgbColor: [223, 213, 178]
    },

    {
        
        description: 'Metallic Cream',
        
        rgbColor: [247, 237, 213]
    },

    {
        
        description: 'Util Brown',
        
        rgbColor: [58, 42, 27]
    },

    {
        
        description: 'Util Medium Brown',
        
        rgbColor: [120, 95, 51]
    },

    {
        
        description: 'Util Light Brown',
        
        rgbColor: [181, 160, 121]
    },

    {
        
        description: 'Metallic White',
        
        rgbColor: [255, 255, 246]
    },

    {
        
        description: 'Metallic Frost White',
        
        rgbColor: [234, 234, 234]
    },

    {
        
        description: 'Worn Honey Beige',
        
        rgbColor: [176, 171, 148]
    },

    {
        
        description: 'Worn Brown',
        
        rgbColor: [69, 56, 49]
    },

    {
        
        description: 'Worn Dark Brown',
        
        rgbColor: [42, 40, 43]
    },

    {
        
        description: 'Worn straw beige',
        
        rgbColor: [114, 108, 87]
    },

    {
        
        description: 'Brushed Steel',
        
        rgbColor: [106, 116, 124]
    },

    {
        
        description: 'Brushed Black steel',
        
        rgbColor: [53, 65, 88]
    },

    {
        
        description: 'Brushed Aluminium',
        
        rgbColor: [155, 160, 168]
    },

    {
        
        description: 'Chrome',
        
        rgbColor: [88, 112, 161]
    },

    {
        
        description: 'Worn Off White',
        
        rgbColor: [234, 230, 222]
    },

    {
        
        description: 'Util Off White',
        
        rgbColor: [223, 221, 208]
    },

    {
        
        description: 'Worn Orange',
        
        rgbColor: [242, 173, 46]
    },

    {
        
        description: 'Worn Light Orange',
        
        rgbColor: [249, 164, 88]
    },

    {
        
        description: 'Metallic Securicor Green',
        
        rgbColor: [131, 197, 102]
    },

    {
        
        description: 'Worn Taxi Yellow',
        
        rgbColor: [241, 204, 64]
    },

    {
        
        description: 'police car blue',
        
        rgbColor: [76, 195, 218]
    },

    {
        
        description: 'Matte Green',
        
        rgbColor: [78, 100, 67]
    },

    {
        
        description: 'Matte Brown',
        
        rgbColor: [188, 172, 143]
    },

    {
        
        description: 'Worn Orange',
        
        rgbColor: [248, 182, 88]
    },

    {
        
        description: 'Matte White',
        
        rgbColor: [252, 249, 241]
    },

    {
        
        description: 'Worn White',
        
        rgbColor: [255, 255, 251]
    },

    {
        
        description: 'Worn Olive Army Green',
        
        rgbColor: [129, 132, 76]
    },

    {
        
        description: 'Pure White',
        
        rgbColor: [255, 255, 255]
    },

    {
        
        description: 'Hot Pink',
        
        rgbColor: [242, 31, 153]
    },

    {
        
        description: 'Salmon pink',
        
        rgbColor: [253, 214, 205]
    },

    {
        
        description: 'Metallic Vermillion Pink',
        
        rgbColor: [223, 88, 145]
    },

    {
        
        description: 'Orange',
        
        rgbColor: [246, 174, 32]
    },

    {
        
        description: 'Green',
        
        rgbColor: [176, 238, 110]
    },

    {
        
        description: 'Blue',
        
        rgbColor: [8, 233, 250]
    },

    {
        
        description: 'Mettalic Black Blue',
        
        rgbColor: [10, 12, 23]
    },

    {
        
        description: 'Metallic Black Purple',
        
        rgbColor: [12, 13, 24]
    },

    {
        
        description: 'Metallic Black Red',
        
        rgbColor: [14, 13, 20]
    },

    {
        
        description: 'hunter green',
        
        rgbColor: [159, 158, 138]
    },

    {
        
        description: 'Metallic Purple',
        
        rgbColor: [98, 18, 118]
    },

    {
        
        description: 'Metaillic V Dark Blue',
        
        rgbColor: [11, 20, 33]
    },

    {
        
        description: 'MODSHOP BLACK1',
        
        rgbColor: [17, 20, 26]
    },

    {
        
        description: 'Matte Purple',
        
        rgbColor: [107, 31, 123]
    },

    {
        
        description: 'Matte Dark Purple',
        
        rgbColor: [30, 29, 34]
    },

    {
        
        description: 'Metallic Lava Red',
        
        rgbColor: [188, 25, 23]
    },

    {
        
        description: 'Matte Forest Green',
        
        rgbColor: [45, 54, 42]
    },

    {
        
        description: 'Matte Olive Drab',
        
        rgbColor: [105, 103, 72]
    },

    {
        
        description: 'Matte Desert Brown',
        
        rgbColor: [122, 108, 85]
    },

    {
        
        description: 'Matte Desert Tan',
        
        rgbColor: [195, 180, 146]
    },

    {
        
        description: 'Matte Foilage Green',
        
        rgbColor: [90, 99, 82]
    },

    {
        
        description: 'DEFAULT ALLOY COLOR',
        
        rgbColor: [129, 130, 127]
    },

    {
        
        description: 'Epsilon Blue',
        
        rgbColor: [175, 214, 228]
    },

    {
        
        description: 'Pure Gold',
        
        rgbColor: [122, 100, 64]
    },

    {
        
        description: 'Brushed Gold',
        
        rgbColor: [127, 106, 72]
    },

    
]