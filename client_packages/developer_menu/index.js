/* 
    * DEVELOPER MENU (DEV MENU) 
    * Created by Czesiek2000 gitlab.com/Czesiek2000
*/

const vehicles = require("./menu/data/vehicles");
const weapons = require("./menu/data/weapons");
const weather = require("./menu/data/weather");
const peds = require('./menu/data/peds');
const colors = require('./menu/data/colors');

const config = require("./menu/config");

const NativeUI = require("./menu/nativeui");
const Menu = NativeUI.Menu;
const UIMenuItem = NativeUI.UIMenuItem;
const UIMenuListItem = NativeUI.UIMenuListItem;
const Point = NativeUI.Point;
const ItemsCollection = NativeUI.ItemsCollection;

let player = mp.players.local;

let mainMenus = [];

let mainMenu = new Menu(config.title, "", new Point(config.menuX, config.menuY));

mainMenu.AddItem(playerMenu = new UIMenuItem("Player", "Show functions related to player"));
mainMenu.AddItem(vehicleMenus = new UIMenuItem("Vehicle", "Handle vehicles"));
mainMenu.AddItem(weaponMenus = new UIMenuItem("Weapons", "Weapons menu"));
mainMenu.AddItem(timeMenus = new UIMenuItem("Time", "Change game time"));
mainMenu.AddItem(weatherMenus = new UIMenuItem("Weather", "Change game weather"))
mainMenu.AddItem(descriptions = new UIMenuItem("About", "About developer menu"))

mainMenu.Visible = false;
mainMenus.push(mainMenu);

function userInput(maxLength) {
    mp.gui.chat.show(false);

    mp.game.gameplay.displayOnscreenKeyboard(1, "FMMC_KEY_TIP8S", "", "", "", "", "", maxLength);
    while (mp.game.gameplay.updateOnscreenKeyboard() == 0) mp.game.wait(0);

    mp.gui.chat.show(true);
    return mp.game.gameplay.getOnscreenKeyboardResult();
}

// Player menu
let playerMen = new Menu("Player", "", new Point(config.menuX, config.menuY));
let playerItem1 = new UIMenuItem("Name", "Show player name");
let playerItem2 = new UIMenuItem("Coords", "Show player coords");
let playerItem3 = new UIMenuItem("Model", "Show player model");
let playerItem4 = new UIMenuItem("God mod", "Toggle player god mode");
let playerItem5 = new UIMenuItem("Armor", "Add armor to player");
let playerItem6 = new UIMenuItem("HP", "Set player full hp");
let playerItem7 = new UIMenuItem("Teleport", "Teleport to given location");
let playerItem8 = new UIMenuItem("Freeze", "Freeze player position");
let playerItem9 = new UIMenuItem("Kill", "Kill player");
let playerItem10 = new UIMenuItem("Ip", "Get player ip");
let playerItem11 = new UIMenuItem("SCID", "Get player social club id");
let playerItem12 = new UIMenuItem("Super jump", "Enable player super jump");
let playerItem13 = new UIMenuItem("Stamina", "Player won't loose stamina when running");
let playerItem14 = new UIMenuItem("Invisible", "Set player visibility");
let playerItem15 = new UIMenuItem("Night vision", "Set night vision to player");
let playerItem16 = new UIMenuItem("Super run", "Set player super run");
let playerItem17 = new UIMenuItem("Skin", "Change player skin");

playerMen.AddItem(playerItem1);
playerMen.AddItem(playerItem2);
playerMen.AddItem(playerItem3);
playerMen.AddItem(playerItem4);
playerMen.AddItem(playerItem5);
playerMen.AddItem(playerItem6);
playerMen.AddItem(playerItem7);
playerMen.AddItem(playerItem8);
playerMen.AddItem(playerItem9);
playerMen.AddItem(playerItem10);
playerMen.AddItem(playerItem11);
playerMen.AddItem(playerItem12);
playerMen.AddItem(playerItem13);
playerMen.AddItem(playerItem14);
playerMen.AddItem(playerItem15);
playerMen.AddItem(playerItem16);
playerMen.AddItem(playerItem17);

let godMode = false;
let freeze = false;
let superJump = false;
let invisible = false;
let stamina = false;
let nightVision = false;

playerMen.ItemSelect.on((item => {
    
    if (item.Text == "Name") {
        mp.events.callRemote("devMenu:playerName");

    } else if (item.Text == "Coords") {
        mp.events.callRemote("devMenu:coords");

    } else if (item.Text == "Model") {
        mp.gui.chat.push(`model: ${player.getModel()}`);

    } else if (item.Text == "God mod") {
        godMode = !godMode;
        mp.players.local.setInvincible(godMode);
        mp.game.graphics.notify(`god mode ${godMode ? "enabled" : "disabled"}`);
        
    } else if (item.Text == "Armor") {
        mp.events.callRemote("devMenu:addArmor");
    } else if (item.Text == "HP") {
        mp.events.callRemote("devMenu:addHp");
    } else if (item.Text == "Teleport") {
        let x = userInput(20);
        let y = userInput(20);
        let z = userInput(20);
        
        mp.events.callRemote("devMenu:teleport", x, y, z)
    }else if (item.Text == "Freeze") {
        freeze = !freeze;
        let player = mp.players.local;
        player.freezePosition(freeze);
        mp.game.graphics.notify(`Freeze state: ${freeze}`)
    }else if (item.Text == "Kill") {
        mp.events.callRemote("devMenu:kill");
    }else if (item.Text == "Ip") {
        mp.events.callRemote("devMenu:ip");
    }else if (item.Text == "SCID") {
        mp.events.callRemote("devMenu:scid");
    }else if (item.Text == "Super jump") {
        superJump = !superJump;
        if (superJump) {
            mp.events.add('render', () => {
                mp.game.gameplay.setSuperJumpThisFrame(0);
            })
        }else { 
            mp.events.add('render', () => {
                mp.game.gameplay.setSuperJumpThisFrame(1);
            })
        }

        mp.game.graphics.notify(`Super jump ${superJump ? "enabled" : "disabled"}`)

    }else if (item.Text == "Stamina") {
        let player = mp.players.local;
        stamina = !stamina;
        if (stamina) {
            if (player.isSprinting()) {
                mp.game.player.restoreStamina(100);
            }
            mp.game.graphics.notify(`~g~Player won't loose stamina when sprinting`)
        }else {
            mp.game.player.restoreStamina(0);
            mp.game.graphics.notify(`~g~Player will loose stamina when sprinting`)
        }
    }else if (item.Text == "Invisible") {
        invisible = !invisible;
        if (invisible) {
            player.setAlpha(0);
            
        }else {
            player.setAlpha(255);
        }

        mp.game.graphics.notify(`Player is ${invisible ? "invisible" : "visible"}`)
    }else if (item.Text == "Night vision") {
        nightVision = !nightVision
        mp.game.graphics.setNightvision(nightVision);
        mp.game.graphics.notify(`Night vision ${nightVision ? "enabled" : "disabled"}`)
    }else if (item.Text == "Super run") {
        let player = mp.players.local;
        mp.game.invoke("0x6DB47AA77FD94E09", player.handle, 1.49);
        mp.gui.chat.push(`This for now doesn't work, work in progress`)
    }
}))

playerMen.Visible = false;
mainMenus.push(playerMen);

let skinMenu = new Menu("Change Skin", "", new Point(config.menuX, config.menuY));
let sortedPeds = peds.sort();
for(let i = 0; i < sortedPeds.length; i++) {
    skinMenu.AddItem(new UIMenuItem(sortedPeds[i].toUpperCase(), ""));
}

skinMenu.ItemSelect.on((item) => {
    for (let i = 0; i < sortedPeds.length; i++) {
        if(item.Text.toLowerCase() == sortedPeds[i].toLowerCase()){ 
            mp.events.callRemote('devMenu:skinChange', sortedPeds[i]);
            
        }
    }
})

skinMenu.Visible = false;

playerMen.BindMenuToItem(skinMenu, playerItem17);

// Vehicle menu
let vehicleMen = new Menu("Vehicle", "", new Point(config.menuX, config.menuY));
let item1 = new UIMenuItem("Model", "Show vehicle model");
let item2 = new UIMenuItem("Spawn cars")
let item3 = new UIMenuItem("Speed", "Show vehicle speed");
let item4 = new UIMenuItem("Torque", "Torque multiplier");
let item5 = new UIMenuItem("Engine", "Engine multiplier");
let item6 = new UIMenuItem("Reset", "Reset torque");
let item7 = new UIMenuListItem("Open doors", "List of numbers to open specific door in vehicle", new ItemsCollection(["", "Front Right", "Front left", "Back left door", "Back right door", "Hood", "Trunk", "Back", "Back2"]));
let item8 = new UIMenuItem("Close doors", "Close all vehicle doors");
let item9 = new UIMenuItem("Repair", "Repair vehicle that player is in or closest vehicle");
let item10 = new UIMenuItem("Lock", "Lock vehicle");
let item11 = new UIMenuItem("Explode", "Explode closest vehicle");
let item12 = new UIMenuItem("Custom vehicle", "Spawn custom vehicle");
let item13 = new UIMenuItem("Flip", "Flip vehicle when its sitting on roof")
let item14 = new UIMenuListItem("Vehicle seats", "Change vehicle seats", new ItemsCollection(["0","1", "2", "3"]))
let item15 = new UIMenuItem("Freeze vehicle", "Toggle vehicle freeze that player is sitting");
let item16 = new UIMenuItem("Remove vehicle", "Remove closest vehicle to player");
let item17 = new UIMenuItem("Remove from vehicle", "Remove player vehicle that is sitting in");
let item18 = new UIMenuItem("Clean vehicle", "Clean vehicle from dirt");
let item19 = new UIMenuItem("Change plate text", "Change vehicle plate text");
let item20 = new UIMenuListItem("Change plate color", "Change vehicle plate background color", new ItemsCollection(["0","1", "2", "3", "4", "5"]));
let item21 = new UIMenuItem("Put into vehicle", "Put player in vehicle")
let item22 = new UIMenuItem("Vehicle visibility", "Toggle vehicle visibility")
let item23 = new UIMenuItem("Engine state", "Toggle vehicle engine");

vehicleMen.AddItem(item1);
vehicleMen.AddItem(item2);
vehicleMen.AddItem(item3);
vehicleMen.AddItem(item5);
vehicleMen.AddItem(item4);
vehicleMen.AddItem(item6);
vehicleMen.AddItem(item7);
vehicleMen.AddItem(item8);
vehicleMen.AddItem(item9);
vehicleMen.AddItem(item10);
vehicleMen.AddItem(item11);
vehicleMen.AddItem(item12);
vehicleMen.AddItem(item13);
vehicleMen.AddItem(item14);
vehicleMen.AddItem(item15);
vehicleMen.AddItem(item16);
vehicleMen.AddItem(item17);
vehicleMen.AddItem(item18);
vehicleMen.AddItem(item19);
vehicleMen.AddItem(item20);
vehicleMen.AddItem(item21);
vehicleMen.AddItem(item22);
vehicleMen.AddItem(item23);

let vehicleFreeze = false;
let locked = false;
let on = false;
vehicleMen.ItemSelect.on((item) => {
    if (item.Text == "Model") {
        if (player.vehicle) {
            let vehicle = player.vehicle;
            let vehicleName = mp.game.ui.getLabelText(mp.game.vehicle.getDisplayNameFromVehicleModel(vehicle.model));
            mp.game.graphics.notify(`player using ~b~${vehicleName}`);
        } else {
            mp.game.graphics.notify(`Player ~r~not in vehicle`);
        }
    } else if (item.Text == "Speed") {
        if (player.vehicle) {
            vehicle = player.vehicle;
            mp.events.add('render', () => {
                mp.game.ui.setTextEntry("STRING");
                mp.game.ui.addTextComponentSubstringPlayerName(`${(vehicle.getSpeed() * 3.6).toFixed(0)} ~b~KM/H`);
                mp.game.ui.setTextFont(4);
                mp.game.ui.setTextScale(0.99, 0.99);
                mp.game.ui.setTextColour(255, 255, 255, 255);
                mp.game.invoke(0x2513DFB0FB8400FE);
                alignRight = false;
                if (alignRight) {
                    mp.game.ui.setTextRightJustify(true);
                    mp.game.ui.setTextWrap(0, 0.55);
                }

                mp.game.ui.drawText(0.900, 0.750);
            });

        } else {
            mp.game.graphics.notify("player not in vehicle");

        }
    } else if (item.Text == "Engine") {
        mp.events.add('render', () => {
            if (mp.players.local.vehicle) {
                let random = mp.game.gameplay.getRandomFloatInRange('1000', '10000000000');
                mp.game.graphics.notify(`speed ${random}`);
                
                // mp.players.local.vehicle.setEnginePowerMultiplier(random);
                mp.players.local.vehicle.setEnginePowerMultiplier(300)
            }
        })
    } else if (item.Text == "Torque") {
        mp.events.add('render', () => {
            mp.game.graphics.notify(`torque enabled`);
            let vehicle = mp.players.local.vehicle;

            if (player.vehicle) {
                vehicle.setEngineTorqueMultiplier(10.5); // Multiplies by 1.5 the engine torque
            }

        });
    } else if (item.Text == "Reset") {
        let vehicle = mp.players.local.vehicle;
        if (player.vehicle) {
            vehicle.setEngineTorqueMultiplier(1.0); // Multiplies by 1.5 the engine torque
            mp.players.local.vehicle.setEnginePowerMultiplier(1.0);
            mp.game.streaming.notify("Reset torque and engine");
        }
    } else if (item.Text == "Close doors") {
        if (player.vehicle) {
            for (let index = 0; index < 7; index++) {
                mp.players.local.vehicle.setDoorShut(index, true);
            }
        }
    } else if (item.Text == "Repair") {
        if (player.vehicle) {
            mp.events.callRemote("devMenu:repairVehicle", player.vehicle);

        } else {
            let idVehicle = mp.game.vehicle.getClosestVehicle(player.position.x, player.position.y, player.position.z, 1000, 0, 70)
            let vehicle = mp.vehicles.atHandle(idVehicle)
            mp.events.callRemote("devMenu:repairVehicle", vehicle);

        }
    } else if (item.Text == "Lock") {
        locked = !locked;
        let idVehicle = mp.game.vehicle.getClosestVehicle(player.position.x, player.position.y, player.position.z, 10, 0, 70)
        let vehicle = mp.vehicles.atHandle(idVehicle)
        mp.events.callRemote("devMenu:lockVehicle", vehicle, locked);
        mp.game.graphics.notify(`${vehicle.model} is ${locked ? "locked" : "unlocked"}`)

    } else if (item.Text == "Explode") {
        if (player.vehicle) {
            mp.game.graphics.notify("~r~Vehicle won't explode, ~w~exit vehicle and select this option")
        }

        let idVehicle = mp.game.vehicle.getClosestVehicle(player.position.x, player.position.y, player.position.z, 10, 0, 70)
        let vehicle = mp.vehicles.atHandle(idVehicle)
        mp.events.callRemote("devMenu:explodeVehicle", vehicle);

    }else if (item.Text == "Custom vehicle") {
        let model = userInput(32);
        if (model.length > 0) {
            if (!mp.game.streaming.isModelAVehicle(mp.game.joaat(model))) {
                mp.game.graphics.notify("Entered model was not a ~r~vehicle model.");
                return;
            }

            mp.events.callRemote("devMenu:spawnCar", model);
        }
    }else if (item.Text == "Flip") {
        mp.events.callRemote("devMenu:flip");

    }else if (item.Text == "Freeze vehicle") {
        vehicleFreeze = !vehicleFreeze;
        if (player.vehicle) {
            player.vehicle.freezePosition(vehicleFreeze)
        }
    }else if (item.Text == "Remove vehicle") {
        let idVehicle = mp.game.vehicle.getClosestVehicle(player.position.x, player.position.y, player.position.z, 1000, 0, 70)
        let vehicle = mp.vehicles.atHandle(idVehicle)
        mp.events.callRemote("devMenu:destroy", vehicle)

    }else if (item.Text == "Remove from vehicle") {
        if (player.vehicle) {
            mp.events.callRemote("devMenu:removeFromVehicle");
        }else {
            mp.gui.chat.push("Cannot remove you from vehicle, beacause you are not in vehicle")
        }
    }else if (item.Text == "Clean vehicle") {
        mp.events.add('entityStreamIn', (entity) => {
            if (entity.type === 'vehicle' && entity.hasVariable('dirtLevel')) entity.getVariable('dirtLevel') ? entity.setDirtLevel(parseFloat(entity.getVariable('dirtLevel'))) : entity.setDirtLevel(0);
        });
    }else if (item.Text == "Change plate text") {
        let text = userInput(8);
        mp.events.callRemote("devMenu:plateText", text);
    }else if (item.Text == "Put into vehicle") {
        let idVehicle = mp.game.vehicle.getClosestVehicle(player.position.x, player.position.y, player.position.z, 100, 0, 70)
        let vehicle = mp.vehicles.atHandle(idVehicle)
        mp.events.callRemote("devMenu:putInto", vehicle);
    }else if (item.Text == "Vehicle invisible") {
        invisible = !invisible;
        if (invisible) {
            player.vehicle.setAlpha(0);
        }else {
            player.vehicle.setAlpha(255);
        }
        mp.game.graphics.notify(`Invisible ${invisible ? "enabled" : "disabled"}`)
    }else if (item.Text == "Engine state") {
        on = !on;
        mp.events.callRemote("devMenu:engine", on);
    }
})

let spawnMenu = new Menu("Cars", "", new Point(config.menuX, config.menuY));
vehicleMen.BindMenuToItem(spawnMenu, item2);

for(let i = 0; i < vehicles.length; i++) {
    spawnMenu.AddItem(new UIMenuItem(vehicles[i], ""));
}

spawnMenu.ItemSelect.on((item) => {
    for (let i = 0; i < vehicles.length; i++) {
        if(item.Text == vehicles[i]){ 
            mp.events.callRemote('devMenu:spawnCar', vehicles[i]);
            
        }
    }
})

spawnMenu.Visible = false;

vehicleMen.ItemSelect.on((item, index) => {
    switch (item) {
        case item14:
            if (player.vehicle) {
                let seat = parseInt(item.SelectedItem.DisplayText);
                mp.events.callRemote("devMenu:seats", seat);
            }
            break;
        case item20: 
            if (player.vehicle) {
                let plateType = parseInt(item.SelectedItem.DisplayText);
                mp.events.callRemote("devMenu:plateType", plateType); 
            }else {
                mp.gui.chat.push("You must be inside vehicle to change plates");
            }
            break;
    }
})

vehicleMen.ListChange.on((item, index) => {
    switch (item) {
        case item7:
            if (player.vehicle) {
                if (item.SelectedItem.DisplayText == "Front Right") {
                    mp.players.local.vehicle.setDoorOpen(0, false, false);
                } else if (item7.SelectedItem.DisplayText == "Front left") {
                    player.vehicle.setDoorOpen(1, false, false);
                } else if (item7.SelectedItem.DisplayText == "Back left door") {
                    player.vehicle.setDoorOpen(2, false, false);
                } else if (item7.SelectedItem.DisplayText == "Back right door") {
                    player.vehicle.setDoorOpen(3, false, false);
                } else if (item7.SelectedItem.DisplayText == "Hood") {
                    player.vehicle.setDoorOpen(4, false, false);
                } else if (item7.SelectedItem.DisplayText == "Trunk") {
                    player.vehicle.setDoorOpen(5, false, false);
                } else if (item7.SelectedItem.DisplayText == "Back") {
                    player.vehicle.setDoorOpen(6, false, false);
                } else if (item7.SelectedItem.DisplayText == "Back2") {
                    player.vehicle.setDoorOpen(7, false, false);
                }
            }
            break;
    }
})


vehicleMen.Visible = false;
mainMenus.push(vehicleMen);


let weaponMenu = new Menu("Weapon", "", new Point(config.menuX, config.menuY));
let weaponItem1 = new UIMenuItem("Give weapon", "Give player weapon");
let weaponItem2 = new UIMenuItem("All weapons", "Give player all weapons");
let weaponItem3 = new UIMenuItem("Remove weapons", "Remove all weapons from player");
let weaponItem4 = new UIMenuItem("Explosive ammo", "Remove all weapons from player");
let weaponItem5 = new UIMenuItem("Explosive mele", "Remove all weapons from player");
let weaponItem6 = new UIMenuItem("Custom weapon", "Give weapon that user type");
let weaponItem7 = new UIMenuItem("Infinite ammo", "Give player infinite ammo");
let weaponItem8 = new UIMenuItem("Refill ammo", "Refill player");
let weaponItem9 = new UIMenuItem("Remove ammo", "Reset player ammo");

weaponMenu.AddItem(weaponItem1);
weaponMenu.AddItem(weaponItem2);
weaponMenu.AddItem(weaponItem3);
weaponMenu.AddItem(weaponItem4);
weaponMenu.AddItem(weaponItem5);
weaponMenu.AddItem(weaponItem6);
weaponMenu.AddItem(weaponItem7);
weaponMenu.AddItem(weaponItem8);
weaponMenu.AddItem(weaponItem9);

let weaponsMenu = new Menu("Weapons", "", new Point(config.menuX, config.menuY));
for(let i = 0; i < weapons.length; i++) {
    weaponsMenu.AddItem(new UIMenuItem(weapons[i].name, ""));
}

weaponsMenu.ItemSelect.on((item) => {
    for (let i = 0; i < weapons.length; i++) {
        if(item.Text == weapons[i].name){ 
            mp.events.callRemote('devMenu:giveWeapons', weapons[i].value);
            
        }
    }
})

weaponsMenu.Visible = false;

weaponMenu.BindMenuToItem(weaponsMenu, weaponItem1);

weaponMenu.ItemSelect.on((item) => {
    if (item.Text == "All weapons") {
        weapons.forEach(weapon => {
            mp.events.callRemote("devMenu:giveWeapons", weapon.value);
        });
    } else if (item.Text == "Remove weapons") {
        mp.events.callRemote("devMenu:removeWeapons");
    } else if (item.Text == "Explosive ammo") {
        mp.events.add('render', () => {
            mp.game.gameplay.setExplosiveAmmoThisFrame(0);
        })
        mp.gui.chat.push("explosive ammo set");
    } else if (item.Text == "Explosive mele") {
        mp.gui.chat.push("explosive mele set");
        mp.events.add('render', () => {
            mp.game.gameplay.setExplosiveMeleeThisFrame(0);
        })
    }else if (item.Text == "Custom weapon") {
        let weapon = userInput(20);
        mp.events.callRemote("devMenu:giveWeapons", weapon);
    }else if (item.Text == "Infinite ammo") {
        mp.events.callRemote("devMenu:infiniteAmmo")
    }else if (item.Text == "Refill ammo") {
        mp.events.callRemote("devMenu:refill");
    }else if (item.Text == "Remove ammo") {
        mp.events.callRemote("devMenu:removeAmmo")
    }

})

weaponMenu.Visible = false;
mainMenus.push(weaponMenu);

let timeMenu = new Menu("Time menu", "", new Point(config.menuX, config.menuY));
let showt = new UIMenuItem("Show time", "Show game time");
let plus1 = new UIMenuItem("Hour +1", "Toggle one hour forward");
let minus1 = new UIMenuItem("Hour -1", "Toggle one hour backward");
let earlymorning = new UIMenuItem("Set early morning", "Set game time to 6:00");
let morning = new UIMenuItem("Set morning", "Set game time to 9:00");
let noon = new UIMenuItem("Set noon", "Set game time to 12:00");
let earlyafternoon = new UIMenuItem("Set early afternoon", "Set game time to 15:00");
let afternoon = new UIMenuItem("Set afternoon", "Set game time to 18:00");
let evening = new UIMenuItem("Set evening", "Set game time to 21:00");
let midnight = new UIMenuItem("Set midnight", "Set game time to 00:00");
let night = new UIMenuItem("Set night", "Set game time to 03:00");
let freezeTime = new UIMenuItem("Freeze time", "Freeze game time");
let unfreezeTime = new UIMenuItem("Unfreeze time", "Unfreeze game time");
let customTime = new UIMenuItem("Set custom time", "Set custom game time");

timeMenu.AddItem(showt);
timeMenu.AddItem(plus1);
timeMenu.AddItem(minus1);
timeMenu.AddItem(earlymorning);
timeMenu.AddItem(morning);
timeMenu.AddItem(noon);
timeMenu.AddItem(earlyafternoon);
timeMenu.AddItem(afternoon);
timeMenu.AddItem(evening);
timeMenu.AddItem(midnight);
timeMenu.AddItem(night);
timeMenu.AddItem(unfreezeTime);
timeMenu.AddItem(freezeTime);
timeMenu.AddItem(customTime);


timeMenu.ItemSelect.on((item) => {
    if (item.Text == "Show time") {
        mp.events.callRemote("devMenu:time")
        
    }else if (item.Text == "Hour +1") {
        mp.events.callRemote("devMenu:plus");

    }else if (item.Text == "Hour -1") {
        mp.events.callRemote("devMenu:minus")

    }else if (item.Text == "Set early morning") {
        mp.events.callRemote("devMenu:setearlymorning");

    }else if (item.Text == "Set morning") {
        mp.events.callRemote("devMenu:setmorning");

    }else if (item.Text == "Set noon") {
        mp.events.callRemote("devMenu:setnoon");

    }else if (item.Text == "Set early afternoon") {
        mp.events.callRemote("devMenu:setearlyafternoon");

    }else if (item.Text == "Set afternoon") {
        mp.events.callRemote("devMenu:setafternoon");

    }else if (item.Text == "Set evening") {
        mp.events.callRemote("devMenu:setevening");

    }else if (item.Text == "Set midnight") {
        mp.events.callRemote("devMenu:setmidnight");

    }else if (item.Text == "Set night") {
        mp.events.callRemote("devMenu:setnight");
    }else if (item.Text == "Unfreeze time") {
        mp.events.callRemote("devMenu:unfreezetime", config.timeInterval)

    }else if (item.Text == "Freeze time") {
        mp.events.callRemote("devMenu:freezetime");
    }else if (item.Text == "Set custom time") {
        const hour = userInput(2);
        const minute = userInput(2);
        mp.events.callRemote("devMenu:setcustomtime", hour, minute);
    }
})

timeMenu.Visible = false;
mainMenus.push(timeMenu);

let weatherMenu = new Menu("Weather", "", new Point(config.menuX, config.menuY));
let currentWeather = new UIMenuItem("Current weather", "Display current weather type");

for(let i = 0; i < weather.length; i++) {
    weatherMenu.AddItem(new UIMenuItem(weather[i], ""));
}

weatherMenu.ItemSelect.on((item) => {
    for (let i = 0; i < weather.length; i++) {
        if(item.Text == weather[i]){ 
            mp.events.callRemote('devMenu:changeWeather', weather[i]);
            
        }
    }
})

weatherMenu.Visible = false;
mainMenus.push(weatherMenu);

let aboutMenu = new Menu("About menu", "", new Point(config.menuX, config.menuY));
let about = new UIMenuItem("About", "~b~DEVMENU ~w~made by Czesiek2000");
let docs = new UIMenuItem("Version", "Current version is 1.0.1 and in alpha");
let website = new UIMenuItem("Update DevMenu", "For new version of DEVMENU check website: https://gitlab.com/Czesiek2000/developer_menu");

aboutMenu.AddItem(about);
aboutMenu.AddItem(docs);
aboutMenu.AddItem(website);

aboutMenu.Visible = false;
mainMenus.push(aboutMenu);

///////////////////////////////////////////////////////////////////////////////////////////////////

mainMenu.BindMenuToItem(vehicleMen, vehicleMenus);
mainMenu.BindMenuToItem(weaponMenu, weaponMenus);
mainMenu.BindMenuToItem(playerMen, playerMenu);
mainMenu.BindMenuToItem(timeMenu, timeMenus);
mainMenu.BindMenuToItem(weatherMenu, weatherMenus);
mainMenu.BindMenuToItem(aboutMenu, descriptions);


mp.keys.bind(0x75, false, () => { // F6 to open
    if (mainMenu.Visible | vehicleMen.Visible | weaponMenu.Visible | playerMen.Visible | timeMenu.Visible | weatherMenu.Visible | aboutMenu.Visible) {
        mainMenus.forEach(function (element, index, array) {
            element.Close()
        });
    } else {

        mainMenu.Open();
        // mp.gui.chat.show(false);
        mp.gui.cursor.visible = false;
    }
});

mainMenu.MenuClose.on(() => {
    mp.gui.chat.show(true);
    mp.gui.cursor.visible = false;
});