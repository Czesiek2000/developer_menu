const colors = require('./colors');
mp.events.add("devMenu:playerName", (player) => {
    player.outputChatBox(`Name of current player: ${player.name}`);
})

mp.events.add("devMenu:coords", (player) => {
    let pos = player.position;
    player.outputChatBox(`current position X: ${pos.x}, Y: ${pos.y}, Z: ${pos.z}`);
})

mp.events.add("devMenu:skinChange", (player, skin) => {
    player.model = skin;
    // player.outputChatBox(`player model changed to ${skin}`)
    player.notify(`Model changed to ~b~${skin}`)
})

mp.events.add("devMenu:giveWeapons", (player, weapon) => {
    player.giveWeapon(mp.joaat(weapon), 9999);
    // player.outputChatBox(`Give player ${weapon}`);
    player.notify(`Give player ~g~${weapon}`)
})

mp.events.add("devMenu:spawnCar", (player, car) => {
    // let random = Math.floor(Math.random() * 256);
    // let random2 = Math.floor(Math.random() * 256);
    // let random3 = Math.floor(Math.random() * 256);

    // player.outputChatBox(`random value is ${random}, ${random2}, ${random3}`);
    let vehicle = mp.vehicles.new(mp.joaat(car), new mp.Vector3(player.position.x + 2, player.position.y, player.position.z), {
        color: [
            // [random, random2, random3],
            // [random, random2, random3]
            colors[Math.floor(Math.random() * colors.length)].rgbColor,
            colors[Math.floor(Math.random() * colors.length)].rgbColor
        ]
    });
    
    vehicle.numberPlate = "DEVMENU";

    // player.outputChatBox(`car ${car} spawned`);
    player.notify(`car ~b~${car} ~w~spawned`)
});

mp.events.add('devMenu:carColor', (player, color) => {
    if (player.vehicle) {
        player.vehicle.setColorRGB(color[0], color[1], color[2]);
    }
})

mp.events.add("devMenu:putInto", (player, vehicle) => {
    player.putIntoVehicle(vehicle, 0);
    // player.outputChatBox("Put inside vehicle to driver seat");
    player.notify(`Put inside vehicle to driver seat`)
})

mp.events.add("devMenu:repairVehicle", (player, vehicle) => {
    vehicle.repair();
    vehicle.rotation = new mp.Vector3(0,0,0);
    // player.outputChatBox(`${vehicle.id} repaired`);
    player.notify(`${vehicle.id} ~g~repaired`)
})

mp.events.add("devMenu:lockVehicle", (player, vehicle, _locked) => {
    vehicle.locked = _locked;
    // if (vehicle.locked) {
    //     player.notify(`vehicle ~r~locked`)
    // }else {
    //     player.notify(`vehicle ~g~unlocked`)
    // }
})

mp.events.add("devMenu:explodeVehicle", (player, vehicle) => {
    vehicle.explode();
    player.notify(`vehicle exploded`)
})

mp.events.add("devMenu:flip", (player) => {
    if (player.vehicle) {
        let rotation = player.vehicle.rotation;
        // rotation.y = 0;
        player.vehicle.rotation = new mp.Vector3(0, 0, 0);
        player.notify('fliped');
    }
    else {
        // player.outputChatBox("player not in vehicle or vehicle is in normal position");
        player.notify(`~r~Player not inside vehicle or vehicle is in normal position`)
    }
})


mp.events.add("devMenu:seats", (player, _seat) => {
    vehicle = player.vehicle;
    if (vehicle) {
        player.putIntoVehicle(vehicle, parseInt(_seat))
        // player.outputChatBox(`change seat to ${_seat}`);
        player.notify(`Change seat to ~b~${_seat}`);
    }
})

mp.events.add("devMenu:destroy", (player, vehicle) => {
    vehicle.destroy();
    player.notify('vehicle destroyed');
})

mp.events.add("devMenu:removeFromVehicle", (player) => {
    player.removeFromVehicle();
    // player.outputChatBox("You are removed from vehicle");
    player.notify(`~r~You have been removed from vehicle`);
})

mp.events.add("devMenu:plateType", (player, type) => {
    player.vehicle.numberPlateType = type;
    player.notify(`Change plate type to ${type}`);
})

mp.events.add("devMenu:plateText", (player, plate) => {
    player.vehicle.numberPlate = plate;
    player.notify('~g~change plate text');
})

mp.events.add("devMenu:engine", (player, engine) => {
    player.vehicle.engine = engine;

    if (engine) {
        player.notify(`Engine ~g~on`)
    }else {
        player.notify(`Engine ~r~off`);
    }
})

mp.events.add("devMenu:infiniteAmmo", (player) => {
    setInterval(() => {
        let weapon = player.weapon;
        let ammo = player.getWeaponAmmo(weapon);
        // player.outputChatBox(`weapon has ${ammo} bullets`);
        if (ammo < 9999) {
            player.giveWeapon(weapon, 9999);
            // player.outputChatBox(`${weapon} has less than 1000 bullets`)
        //     // player.outputChatBox(`player get weapon ${weapon}`)
        }
    }, 1000);
    let reloading = player.isReloading; // not player.isReloading(), always false
        if (reloading) {
            player.outputChatBox(`reloading: ${reloading}`);
        }else {
            player.outputChatBox(`not reloading: ${reloading}`);

        }
})

mp.events.add("devMenu:removeAmmo", (player) => {
    let weapon = player.weapon;
    player.giveWeapon(weapon, 0);
    // player.outputChatBox("Reset player ammo")
    player.notify('reset weapon ammo');
});

mp.events.add("devMenu:removeWeapons", (player) => {
    player.removeAllWeapons();
    // player.outputChatBox(`You have taken off all your weapons.`);
    player.notify(`Remove weapons from player`);
})

mp.events.add("devMenu:refill", (player) => {
    let weapon = player.weapon;
    // let ammo = player.getWeaponAmmo(weapon);
    player.giveWeapon(mp.joaat(weapon), 10000);
    player.notify(`refill weapon`)
})


mp.events.add("devMenu:addArmor", (player) => {
    let playerArmour = player.armour;
    player.armour = 100;
    // player.outputChatBox(`player armor was ${playerArmour} and was set to ${100}`)
    player.notify(`player armor was ${playerArmour} and was set to ${100}`);
})

mp.events.add("devMenu:addHp", (player) => {
    let playerHealth = player.health
    player.health = 100;
    // player.outputChatBox(`player armor was ${playerHealth} and was set to ${100}`)
    player.notify(`player hp was ${playerHealth} and was set to ${100}`)
})

mp.events.add("devMenu:kill", (player) => {
    player.health = 0;
    // player.outputChatBox(`${player.name} killed`)
    player.notify(`${player.name} was killed`)
})

mp.events.add("devMenu:ip", (player) => {
    player.outputChatBox(`player ip: ${player.ip}`)
})

mp.events.add("devMenu:scid", (player) => {
    player.outputChatBox(`player social club id: ${player.rgscId}`)
})

mp.events.add("devMenu:teleport", (player, x, y, z) => {
    if (!isNaN(parseFloat(x)) && !isNaN(parseFloat(y)) && !isNaN(parseFloat(z))) {
        player.position = new mp.Vector3(parseFloat(x), parseFloat(y), parseFloat(z));
        player.outputChatBox(`player teleported to coords: X: ${x}, Y:${y} Z:${z}`);
    }
})

function addZero(value) {
    return value < 10 ? `0${value}` : value;
}

mp.events.add("devMenu:time", (player) => {
    const hour = mp.world.time.hour;
    const minute = mp.world.time.minute;
    const second = mp.world.time.second;

    player.outputChatBox(`H: ${hour}, M: ${minute}, S:${second}`)
})

mp.events.add("devMenu:plus", (player) => {
    let hour = mp.world.time.hour;
    mp.world.time.hour = hour += 1
    player.notify(`Time set to ~g~${addZero(mp.world.time.hour)}`);
})

mp.events.add("devMenu:minus", (player) => {
    let hour = mp.world.time.hour;
    mp.world.time.hour = hour -= 1
    player.notify(`Time set to ~b~${addZero(mp.world.time.hour)}`);
})

mp.events.add("devMenu:setearlymorning", (player) => {
    let hour = 6;
    let minute = 0;
    mp.world.time.hour = hour;
    mp.world.time.minute = minute;
    player.notify(`time set to ${addZero(hour)} : ${addZero(minute)}`)
})

mp.events.add("devMenu:setmorning", (player) => {
    let hour = 9
    let minute = 0
    mp.world.time.hour = hour;
    mp.world.time.minute = minute;
    player.notify(`time set to ${addZero(hour)} : ${addZero(minute)}`)
})

mp.events.add("devMenu:setnoon", (player) => {
    let hour = 12
    let minute = 0
    mp.world.time.hour = hour;
    mp.world.time.minute = minute;
    player.notify(`time set to ${hour} : ${addZero(minute)}`)
})

mp.events.add("devMenu:setearlyafternoon", (player) => {
    let hour = 15
    let minute = 0
    mp.world.time.hour = hour;
    mp.world.time.minute = minute;
    player.notify(`time set to ${hour} : ${addZero(minute)}`)
})

mp.events.add("devMenu:setafternoon", (player) => {
    let hour = 18
    let minute = 0
    mp.world.time.hour = hour;
    mp.world.time.minute = minute;
    player.notify(`time set to ${hour} : ${addZero(minute)}`)
})

mp.events.add("devMenu:setevening", (player) => {
    let hour = 21
    let minute = 0
    mp.world.time.hour = hour;
    mp.world.time.minute = minute;
    player.notify(`time set to ${hour} : ${addZero(minute)}`)
})

mp.events.add("devMenu:setmidnight", (player) => {
    let hour = 0
    let minute = 0
    mp.world.time.hour = hour;
    mp.world.time.minute = minute;
    player.notify(`time set to ${addZero(hour)} : ${addZero(minute)}`)
})

mp.events.add("devMenu:setnight", (player) => {
    let hour = 3
    let minute = 0
    mp.world.time.hour = hour;
    mp.world.time.minute = minute;
    player.notify(`time set to ${addZero(hour)} : ${addZero(minute)}`)
})

let interval;
mp.events.add("devMenu:unfreezetime", (player, _time) => {
    let time = parseInt(_time);
    interval = setInterval(() => {
        mp.world.time.minute++
        if (mp.world.time.minute >= 60) {
            mp.world.time.hour++;
            mp.world.time.minute = 0;
        }
        // player.outputChatBox(`H: ${mp.world.time.hour}, M: ${mp.world.time.minute}`)
    }, time);
    player.notify(`Time ~g~unfreeze`);
})

mp.events.add("devMenu:freezetime", (player) => {
    clearInterval(interval);
    player.notify(`Time ~r~freeze`);
    let hour = mp.world.time.hour;
    let minute = mp.world.time.minute;
    // player.outputChatBox(`H: ${hour}, M: ${minute}`)
    player.notify(`Time set to H: ${hour}, M: ${minute}`)
})

mp.events.add("devMenu:setcustomtime", (player, _hour, _minute) => {
    let hour = parseInt(_hour);
    let minute = parseInt(_minute)
    mp.world.time.hour = hour;
    mp.world.time.minute = minute;
    player.notify(`Time set to ${addZero(hour)} : ${addZero(minute)}`)
})

mp.events.add("devMenu:weather", (player) => {
    mp.world.weather = 'SNOW';
    const weather = mp.world.weather;
    // player.outputChatBox(`current weather: ${weather}`);
    player.notify(`Current weather ~b~${weather}`);
})

mp.events.add("devMenu:changeWeather", (player, weather) => {
    mp.world.weather = weather;
    // player.outputChatBox(`Weather was changed to ${weather}`);
    player.notify(`Weather was changed to ${weather}`);
})
