module.exports = [{

        description: 'Metallic Black',
        hexColor: '#0d1116',
        rgbColor: [13, 17, 22]
    },

    {

        description: 'Metallic Graphite Black',
        hexColor: '#1c1d21',
        rgbColor: [28, 29, 33]
    },

    {

        description: 'Metallic Black Steal',
        hexColor: '#32383d',
        rgbColor: [50, 56, 61]
    },

    {

        description: 'Metallic Dark Silver',
        hexColor: '#454b4f',
        rgbColor: [69, 75, 79]
    },

    {

        description: 'Metallic Silver',
        hexColor: '#999da0',
        rgbColor: [153, 157, 160]
    },

    {

        description: 'Metallic Blue Silver',
        hexColor: '#c2c4c6',
        rgbColor: [194, 196, 198]
    },

    {

        description: 'Metallic Steel Gray',
        hexColor: '#979a97',
        rgbColor: [151, 154, 151]
    },

    {

        description: 'Metallic Shadow Silver',
        hexColor: '#637380',
        rgbColor: [99, 115, 128]
    },

    {

        description: 'Metallic Stone Silver',
        hexColor: '#63625c',
        rgbColor: [99, 98, 92]
    },

    {

        description: 'Metallic Midnight Silver',
        hexColor: '#3c3f47',
        rgbColor: [60, 63, 71]
    },

    {

        description: 'Metallic Gun Metal',
        hexColor: '#444e54',
        rgbColor: [68, 78, 84]
    },

    {

        description: 'Metallic Anthracite Grey',
        hexColor: '#1d2129',
        rgbColor: [29, 33, 41]
    },

    {

        description: 'Matte Black',
        hexColor: '#13181f',
        rgbColor: [19, 24, 31]
    },

    {

        description: 'Matte Gray',
        hexColor: '#26282a',
        rgbColor: [38, 40, 42]
    },

    {

        description: 'Matte Light Grey',
        hexColor: '#515554',
        rgbColor: [81, 85, 84]
    },

    {

        description: 'Util Black',
        hexColor: '#151921',
        rgbColor: [21, 25, 33]
    },

    {

        description: 'Util Black Poly',
        hexColor: '#1e2429',
        rgbColor: [30, 36, 41]
    },

    {

        description: 'Util Dark silver',
        hexColor: '#333a3c',
        rgbColor: [51, 58, 60]
    },

    {

        description: 'Util Silver',
        hexColor: '#8c9095',
        rgbColor: [140, 144, 149]
    },

    {

        description: 'Util Gun Metal',
        hexColor: '#39434d',
        rgbColor: [57, 67, 77]
    },

    {

        description: 'Util Shadow Silver',
        hexColor: '#506272',
        rgbColor: [80, 98, 114]
    },

    {

        description: 'Worn Black',
        hexColor: '#1e232f',
        rgbColor: [30, 35, 47]
    },

    {

        description: 'Worn Graphite',
        hexColor: '#363a3f',
        rgbColor: [54, 58, 63]
    },

    {

        description: 'Worn Silver Grey',
        hexColor: '#a0a199',
        rgbColor: [160, 161, 153]
    },

    {

        description: 'Worn Silver',
        hexColor: '#d3d3d3',
        rgbColor: [211, 211, 211]
    },

    {

        description: 'Worn Blue Silver',
        hexColor: '#b7bfca',
        rgbColor: [183, 191, 202]
    },

    {

        description: 'Worn Shadow Silver',
        hexColor: '#778794',
        rgbColor: [119, 135, 148]
    },

    {

        description: 'Metallic Red',
        hexColor: '#c00e1a',
        rgbColor: [192, 14, 26]
    },

    {

        description: 'Metallic Torino Red',
        hexColor: '#da1918',
        rgbColor: [218, 25, 24]
    },

    {

        description: 'Metallic Formula Red',
        hexColor: '#b6111b',
        rgbColor: [182, 17, 27]
    },

    {

        description: 'Metallic Blaze Red',
        hexColor: '#a51e23',
        rgbColor: [165, 30, 35]
    },

    {

        description: 'Metallic Graceful Red',
        hexColor: '#7b1a22',
        rgbColor: [123, 26, 34]
    },

    {

        description: 'Metallic Garnet Red',
        hexColor: '#8e1b1f',
        rgbColor: [142, 27, 31]
    },

    {

        description: 'Metallic Desert Red',
        hexColor: '#6f1818',
        rgbColor: [111, 24, 24]
    },

    {

        description: 'Metallic Cabernet Red',
        hexColor: '#49111d',
        rgbColor: [73, 17, 29]
    },

    {

        description: 'Metallic Candy Red',
        hexColor: '#b60f25',
        rgbColor: [182, 15, 37]
    },

    {

        description: 'Metallic Sunrise Orange',
        hexColor: '#d44a17',
        rgbColor: [212, 74, 23]
    },

    {

        description: 'Metallic Classic Gold',
        hexColor: '#c2944f',
        rgbColor: [194, 148, 79]
    },

    {

        description: 'Metallic Orange',
        hexColor: '#f78616',
        rgbColor: [247, 134, 22]
    },

    {

        description: 'Matte Red',
        hexColor: '#cf1f21',
        rgbColor: [207, 31, 33]
    },

    {

        description: 'Matte Dark Red',
        hexColor: '#732021',
        rgbColor: [115, 32, 33]
    },

    {

        description: 'Matte Orange',
        hexColor: '#f27d20',
        rgbColor: [242, 125, 32]
    },

    {

        description: 'Matte Yellow',
        hexColor: '#ffc91f',
        rgbColor: [255, 201, 31]
    },

    {

        description: 'Util Red',
        hexColor: '#9c1016',
        rgbColor: [156, 16, 22]
    },

    {

        description: 'Util Bright Red',
        hexColor: '#de0f18',
        rgbColor: [222, 15, 24]
    },

    {

        description: 'Util Garnet Red',
        hexColor: '#8f1e17',
        rgbColor: [143, 30, 23]
    },

    {

        description: 'Worn Red',
        hexColor: '#a94744',
        rgbColor: [169, 71, 68]
    },

    {

        description: 'Worn Golden Red',
        hexColor: '#b16c51',
        rgbColor: [177, 108, 81]
    },

    {

        description: 'Worn Dark Red',
        hexColor: '#371c25',
        rgbColor: [55, 28, 37]
    },

    {

        description: 'Metallic Dark Green',
        hexColor: '#132428',
        rgbColor: [19, 36, 40]
    },

    {

        description: 'Metallic Racing Green',
        hexColor: '#122e2b',
        rgbColor: [18, 46, 43]
    },

    {

        description: 'Metallic Sea Green',
        hexColor: '#12383c',
        rgbColor: [18, 56, 60]
    },

    {

        description: 'Metallic Olive Green',
        hexColor: '#31423f',
        rgbColor: [49, 66, 63]
    },

    {

        description: 'Metallic Green',
        hexColor: '#155c2d',
        rgbColor: [21, 92, 45]
    },

    {

        description: 'Metallic Gasoline Blue Green',
        hexColor: '#1b6770',
        rgbColor: [27, 103, 112]
    },

    {

        description: 'Matte Lime Green',
        hexColor: '#66b81f',
        rgbColor: [102, 184, 31]
    },

    {

        description: 'Util Dark Green',
        hexColor: '#22383e',
        rgbColor: [34, 56, 62]
    },

    {

        description: 'Util Green',
        hexColor: '#1d5a3f',
        rgbColor: [29, 90, 63]
    },

    {

        description: 'Worn Dark Green',
        hexColor: '#2d423f',
        rgbColor: [45, 66, 63]
    },

    {

        description: 'Worn Green',
        hexColor: '#45594b',
        rgbColor: [69, 89, 75]
    },

    {

        description: 'Worn Sea Wash',
        hexColor: '#65867f',
        rgbColor: [101, 134, 127]
    },

    {

        description: 'Metallic Midnight Blue',
        hexColor: '#222e46',
        rgbColor: [34, 46, 70]
    },

    {

        description: 'Metallic Dark Blue',
        hexColor: '#233155',
        rgbColor: [35, 49, 85]
    },

    {

        description: 'Metallic Saxony Blue',
        hexColor: '#304c7e',
        rgbColor: [48, 76, 126]
    },

    {

        description: 'Metallic Blue',
        hexColor: '#47578f',
        rgbColor: [71, 87, 143]
    },

    {

        description: 'Metallic Mariner Blue',
        hexColor: '#637ba7',
        rgbColor: [99, 123, 167]
    },

    {

        description: 'Metallic Harbor Blue',
        hexColor: '#394762',
        rgbColor: [57, 71, 98]
    },

    {

        description: 'Metallic Diamond Blue',
        hexColor: '#d6e7f1',
        rgbColor: [214, 231, 241]
    },

    {

        description: 'Metallic Surf Blue',
        hexColor: '#76afbe',
        rgbColor: [118, 175, 190]
    },

    {

        description: 'Metallic Nautical Blue',
        hexColor: '#345e72',
        rgbColor: [52, 94, 114]
    },

    {

        description: 'Metallic Bright Blue',
        hexColor: '#0b9cf1',
        rgbColor: [11, 156, 241]
    },

    {

        description: 'Metallic Purple Blue',
        hexColor: '#2f2d52',
        rgbColor: [47, 45, 82]
    },

    {

        description: 'Metallic Spinnaker Blue',
        hexColor: '#282c4d',
        rgbColor: [40, 44, 77]
    },

    {

        description: 'Metallic Ultra Blue',
        hexColor: '#2354a1',
        rgbColor: [35, 84, 161]
    },

    {

        description: 'Metallic Bright Blue',
        hexColor: '#6ea3c6',
        rgbColor: [110, 163, 198]
    },

    {

        description: 'Util Dark Blue',
        hexColor: '#112552',
        rgbColor: [17, 37, 82]
    },

    {

        description: 'Util Midnight Blue',
        hexColor: '#1b203e',
        rgbColor: [27, 32, 62]
    },

    {

        description: 'Util Blue',
        hexColor: '#275190',
        rgbColor: [39, 81, 144]
    },

    {

        description: 'Util Sea Foam Blue',
        hexColor: '#608592',
        rgbColor: [96, 133, 146]
    },

    {

        description: 'Util Lightning blue',
        hexColor: '#2446a8',
        rgbColor: [36, 70, 168]
    },

    {

        description: 'Util Maui Blue Poly',
        hexColor: '#4271e1',
        rgbColor: [66, 113, 225]
    },

    {

        description: 'Util Bright Blue',
        hexColor: '#3b39e0',
        rgbColor: [59, 57, 224]
    },

    {

        description: 'Matte Dark Blue',
        hexColor: '#1f2852',
        rgbColor: [31, 40, 82]
    },

    {

        description: 'Matte Blue',
        hexColor: '#253aa7',
        rgbColor: [37, 58, 167]
    },

    {

        description: 'Matte Midnight Blue',
        hexColor: '#1c3551',
        rgbColor: [28, 53, 81]
    },

    {

        description: 'Worn Dark blue',
        hexColor: '#4c5f81',
        rgbColor: [76, 95, 129]
    },

    {

        description: 'Worn Blue',
        hexColor: '#58688e',
        rgbColor: [88, 104, 142]
    },

    {

        description: 'Worn Light blue',
        hexColor: '#74b5d8',
        rgbColor: [116, 181, 216]
    },

    {

        description: 'Metallic Taxi Yellow',
        hexColor: '#ffcf20',
        rgbColor: [255, 207, 32]
    },

    {

        description: 'Metallic Race Yellow',
        hexColor: '#fbe212',
        rgbColor: [251, 226, 18]
    },

    {

        description: 'Metallic Bronze',
        hexColor: '#916532',
        rgbColor: [145, 101, 50]
    },

    {

        description: 'Metallic Yellow Bird',
        hexColor: '#e0e13d',
        rgbColor: [224, 225, 61]
    },

    {

        description: 'Metallic Lime',
        hexColor: '#98d223',
        rgbColor: [152, 210, 35]
    },

    {

        description: 'Metallic Champagne',
        hexColor: '#9b8c78',
        rgbColor: [155, 140, 120]
    },

    {

        description: 'Metallic Pueblo Beige',
        hexColor: '#503218',
        rgbColor: [80, 50, 24]
    },

    {

        description: 'Metallic Dark Ivory',
        hexColor: '#473f2b',
        rgbColor: [71, 63, 43]
    },

    {

        description: 'Metallic Choco Brown',
        hexColor: '#221b19',
        rgbColor: [34, 27, 25]
    },

    {

        description: 'Metallic Golden Brown',
        hexColor: '#653f23',
        rgbColor: [101, 63, 35]
    },

    {

        description: 'Metallic Light Brown',
        hexColor: '#775c3e',
        rgbColor: [119, 92, 62]
    },

    {

        description: 'Metallic Straw Beige',
        hexColor: '#ac9975',
        rgbColor: [172, 153, 117]
    },

    {

        description: 'Metallic Moss Brown',
        hexColor: '#6c6b4b',
        rgbColor: [108, 107, 75]
    },

    {

        description: 'Metallic Biston Brown',
        hexColor: '#402e2b',
        rgbColor: [64, 46, 43]
    },

    {

        description: 'Metallic Beechwood',
        hexColor: '#a4965f',
        rgbColor: [164, 150, 95]
    },

    {

        description: 'Metallic Dark Beechwood',
        hexColor: '#46231a',
        rgbColor: [70, 35, 26]
    },

    {

        description: 'Metallic Choco Orange',
        hexColor: '#752b19',
        rgbColor: [117, 43, 25]
    },

    {

        description: 'Metallic Beach Sand',
        hexColor: '#bfae7b',
        rgbColor: [191, 174, 123]
    },

    {

        description: 'Metallic Sun Bleeched Sand',
        hexColor: '#dfd5b2',
        rgbColor: [223, 213, 178]
    },

    {

        description: 'Metallic Cream',
        hexColor: '#f7edd5',
        rgbColor: [247, 237, 213]
    },

    {

        description: 'Util Brown',
        hexColor: '#3a2a1b',
        rgbColor: [58, 42, 27]
    },

    {

        description: 'Util Medium Brown',
        hexColor: '#785f33',
        rgbColor: [120, 95, 51]
    },

    {

        description: 'Util Light Brown',
        hexColor: '#b5a079',
        rgbColor: [181, 160, 121]
    },

    {

        description: 'Metallic White',
        hexColor: '#fffff6',
        rgbColor: [255, 255, 246]
    },

    {

        description: 'Metallic Frost White',
        hexColor: '#eaeaea',
        rgbColor: [234, 234, 234]
    },

    {

        description: 'Worn Honey Beige',
        hexColor: '#b0ab94',
        rgbColor: [176, 171, 148]
    },

    {

        description: 'Worn Brown',
        hexColor: '#453831',
        rgbColor: [69, 56, 49]
    },

    {

        description: 'Worn Dark Brown',
        hexColor: '#2a282b',
        rgbColor: [42, 40, 43]
    },

    {

        description: 'Worn straw beige',
        hexColor: '#726c57',
        rgbColor: [114, 108, 87]
    },

    {

        description: 'Brushed Steel',
        hexColor: '#6a747c',
        rgbColor: [106, 116, 124]
    },

    {

        description: 'Brushed Black steel',
        hexColor: '#354158',
        rgbColor: [53, 65, 88]
    },

    {

        description: 'Brushed Aluminium',
        hexColor: '#9ba0a8',
        rgbColor: [155, 160, 168]
    },

    {

        description: 'Chrome',
        hexColor: '#5870a1',
        rgbColor: [88, 112, 161]
    },

    {

        description: 'Worn Off White',
        hexColor: '#eae6de',
        rgbColor: [234, 230, 222]
    },

    {

        description: 'Util Off White',
        hexColor: '#dfddd0',
        rgbColor: [223, 221, 208]
    },

    {

        description: 'Worn Orange',
        hexColor: '#f2ad2e',
        rgbColor: [242, 173, 46]
    },

    {

        description: 'Worn Light Orange',
        hexColor: '#f9a458',
        rgbColor: [249, 164, 88]
    },

    {

        description: 'Metallic Securicor Green',
        hexColor: '#83c566',
        rgbColor: [131, 197, 102]
    },

    {

        description: 'Worn Taxi Yellow',
        hexColor: '#f1cc40',
        rgbColor: [241, 204, 64]
    },

    {

        description: 'police car blue',
        hexColor: '#4cc3da',
        rgbColor: [76, 195, 218]
    },

    {

        description: 'Matte Green',
        hexColor: '#4e6443',
        rgbColor: [78, 100, 67]
    },

    {

        description: 'Matte Brown',
        hexColor: '#bcac8f',
        rgbColor: [188, 172, 143]
    },

    {

        description: 'Worn Orange',
        hexColor: '#f8b658',
        rgbColor: [248, 182, 88]
    },

    {

        description: 'Matte White',
        hexColor: '#fcf9f1',
        rgbColor: [252, 249, 241]
    },

    {

        description: 'Worn White',
        hexColor: '#fffffb',
        rgbColor: [255, 255, 251]
    },

    {

        description: 'Worn Olive Army Green',
        hexColor: '#81844c',
        rgbColor: [129, 132, 76]
    },

    {

        description: 'Pure White',
        hexColor: '#ffffff',
        rgbColor: [255, 255, 255]
    },

    {

        description: 'Hot Pink',
        hexColor: '#f21f99',
        rgbColor: [242, 31, 153]
    },

    {

        description: 'Salmon pink',
        hexColor: '#fdd6cd',
        rgbColor: [253, 214, 205]
    },

    {

        description: 'Metallic Vermillion Pink',
        hexColor: '#df5891',
        rgbColor: [223, 88, 145]
    },

    {

        description: 'Orange',
        hexColor: '#f6ae20',
        rgbColor: [246, 174, 32]
    },

    {

        description: 'Green',
        hexColor: '#b0ee6e',
        rgbColor: [176, 238, 110]
    },

    {

        description: 'Blue',
        hexColor: '#08e9fa',
        rgbColor: [8, 233, 250]
    },

    {

        description: 'Mettalic Black Blue',
        hexColor: '#0a0c17',
        rgbColor: [10, 12, 23]
    },

    {

        description: 'Metallic Black Purple',
        hexColor: '#0c0d18',
        rgbColor: [12, 13, 24]
    },

    {

        description: 'Metallic Black Red',
        hexColor: '#0e0d14',
        rgbColor: [14, 13, 20]
    },

    {

        description: 'hunter green',
        hexColor: '#9f9e8a',
        rgbColor: [159, 158, 138]
    },

    {

        description: 'Metallic Purple',
        hexColor: '#621276',
        rgbColor: [98, 18, 118]
    },

    {

        description: 'Metaillic V Dark Blue',
        hexColor: '#0b1421',
        rgbColor: [11, 20, 33]
    },

    {

        description: 'MODSHOP BLACK1',
        hexColor: '#11141a',
        rgbColor: [17, 20, 26]
    },

    {

        description: 'Matte Purple',
        hexColor: '#6b1f7b',
        rgbColor: [107, 31, 123]
    },

    {

        description: 'Matte Dark Purple',
        hexColor: '#1e1d22',
        rgbColor: [30, 29, 34]
    },

    {

        description: 'Metallic Lava Red',
        hexColor: '#bc1917',
        rgbColor: [188, 25, 23]
    },

    {

        description: 'Matte Forest Green',
        hexColor: '#2d362a',
        rgbColor: [45, 54, 42]
    },

    {

        description: 'Matte Olive Drab',
        hexColor: '#696748',
        rgbColor: [105, 103, 72]
    },

    {

        description: 'Matte Desert Brown',
        hexColor: '#7a6c55',
        rgbColor: [122, 108, 85]
    },

    {

        description: 'Matte Desert Tan',
        hexColor: '#c3b492',
        rgbColor: [195, 180, 146]
    },

    {

        description: 'Matte Foilage Green',
        hexColor: '#5a6352',
        rgbColor: [90, 99, 82]
    },

    {

        description: 'DEFAULT ALLOY COLOR',
        hexColor: '#81827f',
        rgbColor: [129, 130, 127]
    },

    {

        description: 'Epsilon Blue',
        hexColor: '#afd6e4',
        rgbColor: [175, 214, 228]
    },

    {

        description: 'Pure Gold',
        hexColor: '#7a6440',
        rgbColor: [122, 100, 64]
    },

    {

        description: 'Brushed Gold',
        hexColor: '#7f6a48',
        rgbColor: [127, 106, 72]
    },


]